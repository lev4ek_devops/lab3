from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from tutorials import views
 
urlpatterns = [
    path('tutorials', views.tutorial_list),
    path('tutorials/<int:pk>', views.tutorial_detail),
    path('tutorials/published', views.tutorial_list_published)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
