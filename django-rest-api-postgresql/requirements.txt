asgiref==3.6.0
Django==4.2
django-cors-headers==3.7.0
django-rest-framework==0.1.0
djangorestframework==3.14.0
python-dotenv==0.20.0
psycopg2-binary==2.8.6
pytz==2023.3
sqlparse==0.4.3
gunicorn==20.0.4
whitenoise==5.2.0
